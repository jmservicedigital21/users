// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDvuGhIdaTbB2fre08VWryRZv_AMio05Ww",
    authDomain: "users-8490f.firebaseapp.com",
    databaseURL: "https://users-8490f.firebaseio.com",
    projectId: "users-8490f",
    storageBucket: "users-8490f.appspot.com",
    messagingSenderId: "7965844266",
    appId: "1:7965844266:web:182189706b0fd7d8ce7453"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
