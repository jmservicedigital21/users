import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../services/user.service';
import {Observable, Subscription} from 'rxjs';
import {User} from 'firebase';
import {UserCustom} from '../../models/user';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit, OnDestroy {

 user?: UserCustom;
  uid = '';
  sub: Subscription;


  constructor(private activatedRoute: ActivatedRoute, private userService: UserService) {
    this.sub = this.activatedRoute.paramMap.subscribe(param => {
this.uid = param.get('id') || '';
console.log('uid', this.uid);
this.userService.getUser(this.uid).subscribe((data: UserCustom ) => {
 this.user = data;
 this.user.createdAt = (data.createdAt as any).toDate();
}, err => {
  console.error(err);
});
    })
  }

  ngOnInit(): void {
  }
  ngOnDestroy(): void {
    this.sub.unsubscribe();

  }

}
