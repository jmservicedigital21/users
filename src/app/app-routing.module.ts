import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './component/home/home.component';
import { RegisterComponent } from './component/register/register.component';
import {UserListComponent} from './component/user-list/user-list.component';
import {UserDetailComponent} from './component/user-detail/user-detail.component';

const routes: Routes = [
  {path: '' , component: HomeComponent },
  {path: 'register', component: RegisterComponent },
  {path: 'users', component: UserListComponent },
  {path: 'user-detail/:id', component: UserDetailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
